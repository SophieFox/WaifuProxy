﻿using Fiddler;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Text.RegularExpressions;
using System.Security.Cryptography;
using System.IO;
using System.Web;
using System.Diagnostics;

namespace WaifuProxy
{
    class Program
    {
        internal static int proxyPort = 8080;
        internal static bool keepGoing = true;
        internal static string CacheDirectory = @"cache";

        private static Regex ImageMatcher = new Regex(@"\.(?:jpg|jpeg|png)$");
        private static string WaifuPath = @"waifu2x-caffe\waifu2x-caffe-cui.exe";
        private static string[] WaifuOptions = new string[]
        {
            "-p cudnn", // requires cudnn64_5.dll, see https://developer.nvidia.com/cudnn - otherwise use CUDA with: -p gpu
            "-s 2", // Scale
            "-n 1", "-m auto_scale", // Denoise: 1, method: auto-denoise and scale
            // -i ... -o ...
        };
        private static Semaphore WaifuSem = new Semaphore(2, 2);
        
        static void Main(string[] args)
        {
            Console.CancelKeyPress += new ConsoleCancelEventHandler(Console_CancelKeyPress);
            FiddlerApplication.SetAppDisplayName("WaifuProxy");

            if (!Directory.Exists(CacheDirectory))
                Directory.CreateDirectory(CacheDirectory);

            if (!File.Exists(WaifuPath))
            {
                Console.WriteLine(String.Format("{0} not found! Did you install it correctly?", WaifuPath));
                return;
            }

            Fiddler.FiddlerApplication.OnNotification += delegate (object sender, NotificationEventArgs oNEA) { Console.WriteLine("** NotifyUser: " + oNEA.NotifyString); };
            Fiddler.FiddlerApplication.Log.OnLogString += delegate (object sender, LogEventArgs oLEA) { Console.WriteLine("** LogString: " + oLEA.LogString); };

            Fiddler.FiddlerApplication.BeforeRequest += delegate(Session s)
            {
                if(ImageMatcher.IsMatch(s.fullUrl))
                {
                    if (s.fullUrl.Contains("thumb"))
                        return;

                    // In order to enable response tampering, buffering mode MUST
                    // be enabled; this allows FiddlerCore to permit modification of
                    // the response in the BeforeResponse handler rather than streaming
                    // the response to the client as the response comes in.
                    s.bBufferResponse = true;

                    var uri = new Uri(s.fullUrl);
                    //var filename = uri.Segments[uri.Segments.Count() - 1];
                    var mime = MimeMapping.GetMimeMapping(uri.Segments[uri.Segments.Count() - 1]);
                    var fileExt = GetDefaultExtension(mime);
                    var hash = getUriHash(uri);

                    if(isCached(hash))
                    {
                        FiddlerApplication.Log.LogFormat("{0} *IS* cached and read from disk.", hash);

                        s.utilCreateResponseAndBypassServer();
                        s.oResponse.headers.SetStatus(200, "Ok");
                        s.oResponse["Content-Type"] = mime;
                        s.oResponse["Cache-Control"] = "private, max-age=0";

                        var cachePath = Path.Combine(CacheDirectory, hash + fileExt);
                        s.ResponseBody = File.ReadAllBytes(cachePath);
                    } else
                    {
                        FiddlerApplication.Log.LogFormat("{0} is not cached, it should be cached once we have the response body.", hash);
                    }
                }
            };

            Fiddler.FiddlerApplication.BeforeResponse += delegate (Fiddler.Session s)
            {
                // In order to enable response tampering, buffering mode MUST
                // be enabled; this allows FiddlerCore to permit modification of
                // the response in the BeforeResponse handler rather than streaming
                // the response to the client as the response comes in.
                s.bBufferResponse = true;

                if (ImageMatcher.IsMatch(s.fullUrl))
                {
                    if (s.fullUrl.Contains("thumb"))
                        return;

                    var uri = new Uri(s.fullUrl);
                    //var filename = uri.Segments[uri.Segments.Count() - 1];
                    var mime = MimeMapping.GetMimeMapping(uri.Segments[uri.Segments.Count() - 1]);
                    var fileExt = GetDefaultExtension(mime);
                    var hash = getUriHash(uri);
                    var cachePath = Path.Combine(CacheDirectory, hash + fileExt);

                    if (isCached(hash))
                    {
                        FiddlerApplication.Log.LogFormat("{0} *IS* cached - bypassing Waifu.", hash);
                    } else
                    {
                        FiddlerApplication.Log.LogFormat("{0} is not cached, scaling ...", hash);
                        
                        File.WriteAllBytes(cachePath, s.ResponseBody);

                        // Waifu!
                        waifuScalePic(cachePath);
                    }

                    s.ResponseBody = File.ReadAllBytes(cachePath);
                    FiddlerApplication.Log.LogFormat("Serving {0} ...", cachePath);
                }
             };

                Console.WriteLine(String.Format("Starting {0} ...", FiddlerApplication.GetVersionString()));
            Fiddler.CONFIG.IgnoreServerCertErrors = false;
            FiddlerApplication.Prefs.SetBoolPref("fiddler.network.streaming.abortifclientaborts", true);
            FiddlerCoreStartupFlags startupFlags = FiddlerCoreStartupFlags.Default & ~FiddlerCoreStartupFlags.RegisterAsSystemProxy & ~FiddlerCoreStartupFlags.DecryptSSL;
            FiddlerApplication.Startup(proxyPort, startupFlags);

            FiddlerApplication.Log.LogFormat("Created endpoint listening on port {0}", proxyPort);

            FiddlerApplication.Log.LogFormat("Starting with settings: [{0}]", startupFlags);
            FiddlerApplication.Log.LogFormat("Gateway: {0}", CONFIG.UpstreamGateway.ToString());

            Console.WriteLine("Hit CTRL+C to end session.");

            do
            {
                Thread.Sleep(500);
            } while (keepGoing);

            Fiddler.FiddlerApplication.Shutdown();
            Thread.Sleep(1000);
        }

        private static void waifuScalePic(string cachePath)
        {
            var args = new List<string>();
            args.AddRange(WaifuOptions);
            args.AddRange(new string[]
            {
                "-i " + cachePath,
                "-o " + cachePath
            });
            var pi = new ProcessStartInfo(WaifuPath, string.Join(" ", args.ToArray()));
            pi.CreateNoWindow = true;
            pi.WindowStyle = ProcessWindowStyle.Hidden;
            pi.UseShellExecute = true;

            WaifuSem.WaitOne();
            var p = Process.Start(pi);
            p.WaitForExit();
            WaifuSem.Release();
        }

        private static bool isCached(string hash)
        {
            var files = Directory.EnumerateFiles(CacheDirectory, hash+".*", SearchOption.TopDirectoryOnly);
            var num = files.Count();
            return num != 0;
        }

        private static string getUriHash(Uri uri)
        {
            var hash = (new SHA1Managed()).ComputeHash(Encoding.UTF8.GetBytes(uri.AbsoluteUri));
            return string.Join("", hash.Select(b => b.ToString("x2")).ToArray());
        }

        private static void Console_CancelKeyPress(object sender, ConsoleCancelEventArgs e)
        {
            Program.keepGoing = false;
        }

        // thanks http://stackoverflow.com/a/23087871
        internal static string GetDefaultExtension(string mimeType)
        {
            string result;
            Microsoft.Win32.RegistryKey key;
            object value;

            key = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(@"MIME\Database\Content Type\" + mimeType, false);
            value = key != null ? key.GetValue("Extension", null) : null;
            result = value != null ? value.ToString() : string.Empty;

            return result;
        }
    }
}
